/* Copyright (C) 2024 University of Texas at El Paso

   Contributed by: Christoph Lauter 
                   
                   and the 2024 class of CS4390/5390

		   Applied Numerical Computing for Multimedia
		   Applications.

   All rights reserved.

   NO LICENSE SPECIFIED.

*/

#ifndef UTEPNUM_H
#define UTEPNUM_H

#include "integer_ops.h"
#include "widefloat_ops.h"

#endif

